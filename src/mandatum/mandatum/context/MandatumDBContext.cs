﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using mandatum.models;
using Microsoft.EntityFrameworkCore;

namespace mandatum.context
{
    public class MandatumDBContext : DbContext
    {
        public MandatumDBContext(DbContextOptions<MandatumDBContext> options) : base(options)
        {
            this.Database.EnsureCreated();
        }

        #region model.facility
        public DbSet<BasisStation> BasisStations { get; set; }
        public DbSet<EndStation> EndStations { get; set; }
        public DbSet<FactoryStation> FactoryStations { get; set; }
        public DbSet<Table> Tables { get; set; }
        #endregion
        #region model.management
        public DbSet<Order> Orders { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<SalesControlUnit> SalesControlUnits { get; set; }
        #endregion
        #region model.products
        public DbSet<Article> Articles { get; set; }
        public DbSet<CustomArticle> CustomArticles { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<PartialArticle> PartialArticles { get; set; }
        #endregion
        #region model.security
        public DbSet<Chef> Chefs { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlite("Filename=D:\\mandatum.db", options =>
            //{
            //    options.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName);
            //});

            base.OnConfiguring(optionsBuilder);
        }

        

        
    }
}
