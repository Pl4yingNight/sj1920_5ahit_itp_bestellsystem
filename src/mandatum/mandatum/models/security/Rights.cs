﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mandatum.models
{
    [Flags]
    public enum Rights:int
    {
        NONE = 0,
        CAN_ORDER = 1,
        CAN_COOK = 2,
        CAN_ADD_USER = 4,
        CAN_REMOVE_USER = 8
    }
}