﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mandatum.models
{
    public class User : AEntity
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }

        public Role Role
        {
            get => default;
            set
            {
            }
        }

        public List<Order> Orders
        {
            get => default;
            set
            {
            }
        }

        public List<Table> Tables
        {
            get => default;
            set
            {
            }
        }
    }
}