using System.ComponentModel.DataAnnotations;

namespace mandatum.models.security.user
{
    public class AuthenticateModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}