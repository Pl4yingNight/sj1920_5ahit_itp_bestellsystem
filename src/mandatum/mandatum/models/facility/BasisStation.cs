﻿using System;
using System.Collections.Generic;
using System.Text;

namespace mandatum.models
{
    public class BasisStation : AEntity
    {
        public List<Article> Articles
        {
            get => default;
            set
            {
            }
        }

        public List<Order> Orders
        {
            get => default;
            set
            {
            }
        }

        public List<Ingredient> StoredIngredients
        {
            get => default;
            set
            {
            }
        }

        public string Name
        {
            get => default;
            set
            {
            }
        }

        public string Location
        {
            get => default;
            set
            {
            }
        }

        public string SalesUID
        {
            get => default;
            set
            {
            }
        }

        public List<User> Users
        {
            get => default;
            set
            {
            }
        }
    }
}