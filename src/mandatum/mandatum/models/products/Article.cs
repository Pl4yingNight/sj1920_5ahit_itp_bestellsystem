﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace mandatum.models
{
    public class Article : AEntity
    {

        public string Type
        {
            get => default;
            set
            {
            }
        }

        public SalesControlUnit SalesControlUnit
        {
            get => default;
            set
            {
            }
        }

        public bool Stocked
        {
            get => default;
            set
            {
            }
        }

        public List<PartialArticle> PartialArticles
        {
            get => default;
            set
            {
            }
        }
    }
}