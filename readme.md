# Mandatum (Bestellsystem)
## Ressources:

+ [YouTrack](https://mandatum.myjetbrains.com/youtrack/dashboard)
+ [Planung Dok](https://htlkrems3500-my.sharepoint.com/:w:/r/personal/s_resch_htlkrems_at/_layouts/15/Doc.aspx?sourcedoc=%7B6F4220A9-0FB7-4842-BBA0-32939D4A68B0%7D&file=Projektidee.docx&action=default&mobileredirect=true)
+ [Projekttagebuch](https://htlkrems3500-my.sharepoint.com/:x:/r/personal/s_resch_htlkrems_at/_layouts/15/Doc.aspx?sourcedoc=%7B0E24D039-1AF0-4B5F-9358-4C178E3819DD%7D&file=Projekttagebuch.xlsx&action=default&mobileredirect=true)

## Planung:

+ [Shared Folder](https://htlkrems3500-my.sharepoint.com/:f:/g/personal/s_resch_htlkrems_at/Ejg3nP5CO4dGk7QpbZ_1aZsBR4pMVffiOKg3tkpvZLGybA?e=0TB8EW) PW: mandatum